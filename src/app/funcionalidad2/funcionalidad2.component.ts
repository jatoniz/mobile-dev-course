import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'funcionalidad2',
  templateUrl: './funcionalidad2.component.html',
  moduleId: module.id,
})
export class Funcionalidad2Component implements OnInit {

  constructor(private routerExtensions: RouterExtensions) { }

  ngOnInit() {
  }

  goBack() {
    this.routerExtensions.backToPreviousPage();
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onDrawerButtonClick(): void {    
    this.routerExtensions.navigate(["/funcionalidad1"], {
      transition: {
          name: "fade"
      }
  });
  }

  

}
