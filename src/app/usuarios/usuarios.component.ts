import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as appSettings from "tns-core-modules/application-settings";

@Component({
    selector: "usuarios",
    moduleId: module.id,
    templateUrl: "./usuarios.component.html"
})
export class usuariosComponent implements OnInit {

    nombreUsuario: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
   
    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    onButtonTap(): void{
        console.log(this.nombreUsuario);
        if(this.nombreUsuario.length > 4) {
            this.search.emit(this.nombreUsuario);
            appSettings.setString("nombreUsuario","Hola");
            appSettings.setBoolean("estalogueado", true);
            const estalogueado = appSettings.getBoolean("estalogueado",false);
        }else{
            appSettings.clear();
        }
    }
}
