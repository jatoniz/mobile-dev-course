import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { usuariosRoutingModule } from "./usuarios-routing.module";
import { usuariosComponent } from "./usuarios.component";
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [
        NativeScriptCommonModule,
        usuariosRoutingModule, 
        FormsModule
    ],
    declarations: [
        usuariosComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class usuariosModule { }