import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { SearchFormComponent } from "./search-form.component";
import { MinLenDirective } from "../search/minlen.validator";
import { SettingBusyComponent} from "../search/SettingBusy.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule,
        NativeScriptFormsModule
    ],
    
    declarations: [SearchComponent, SearchFormComponent,MinLenDirective,SettingBusyComponent],


    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }
